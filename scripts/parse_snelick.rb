# Docx::Document instance methods
# bookmarks
# doc
# document_properties
# each_paragraph
# font_size
# paragraphs
# replace_entry
# save
# styles
# tables
# text
# to_html
# to_s
# xml
# zip

require 'docx'
load File.join(__dir__, 'nokogiri_extensions.rb')
load File.join(__dir__, 'extractor.rb')
load File.join(__dir__, 'clear_tables.rb')
load File.join(__dir__, 'headers_footers.rb')
clear_tables
# node.display_structure
# puts; puts Rainbow(node.map_styles).green
sources = []
sources << ['../test_data/HL7v2DataTypeSpecializationsBallot-May2018', 'HL7 v2 DataType Specializations Ballot-May2018']
sources << ['../test_data/HL7v2ConformanceMethodology_14052019_accepted_changes', 'HL7 v2 Conformance Methodology_May-2019']
# sources << ['../test_data/HL7v2ConformanceMethodology_14052019_accepted_changes', 'test']
sources.each do |docx, title|
  extractor = SDoc::DocXtractor.new
  path = File.join(__dir__, docx)
  extractor.extract_rels(path)
  extractor.extract_headers(path)
  extractor.extract_footers(path)
  file = path + '.docx'
  doc = Docx::Document.open(file)
  extractor.extract_document(doc.doc, title)
end